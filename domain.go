package domain_validator

import "strings"

type Domain struct {
	Subdomains []string
	Domain     string
	TLD        string
}

func (d Domain) ToString(includeSubdomains bool) string {
	str := d.Domain + "." + d.TLD
	if len(d.Subdomains) > 0 && includeSubdomains {
		str = strings.Join(d.Subdomains, ".") + "." + str
	}
	return str
}
