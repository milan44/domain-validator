package domain_validator

import (
	"fmt"
	"testing"
)

func TestValidate(t *testing.T) {
	mp := map[string]bool{
		"wiese2.org":                   true,
		"example.wiese2.org":           true,
		"example.subdomain.wiese2.org": true,
		"very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.long.butstillvalid.wiese2.org":                           true,
		"very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.very.long.wiese2.org": false,
		"literally not a domain":       false,
		"aksdj)(/)(/asd.invalid.chars": false,
	}

	for domain, shouldBeValid := range mp {
		d, err := Validate(domain)
		if err == nil != shouldBeValid {
			errStr := "<nil>"
			if err != nil {
				errStr = err.Error()
			}

			fmt.Printf("--- Error\nDomain:          %s\nExpected:        %t\nReturned Error:  %s\nReturned Domain: %s\n", domain, shouldBeValid, errStr, d)
			t.Fail()
		}
	}
}
