package domain_validator

import "strings"

func replaceLast(str, search, replace string) string {
	i := strings.LastIndex(str, search)
	return str[:i] + strings.Replace(str[i:], search, replace, 1)
}
