package domain_validator

import (
	"github.com/pkg/errors"
	"regexp"
	"strings"
)

func Validate(domain string) (*Domain, error) {
	obj := Domain{
		Subdomains: make([]string, 0),
	}

	domain = strings.ToLower(domain)
	domain = strings.TrimSpace(domain)

	if len([]rune(domain)) > 253 {
		return nil, errors.New("whole record is longer than 253 characters")
	}

	tld := checkDoubleTLD(domain)
	if tld != "" {
		obj.TLD = tld
		domain = replaceLast(domain, tld, ".temp")
	}

	parts := strings.Split(domain, ".")
	if len(parts) < 2 {
		return nil, errors.New("missing tld or domain")
	}

	if obj.TLD == "" {
		obj.TLD = parts[len(parts)-1]
	}
	parts = parts[:len(parts)-1]

	rgx := regexp.MustCompile(`(?m)^[^a-z0-9]|[^a-z0-9\-]|[^a-z0-9]$`)
	for _, part := range parts {
		if part == "" || len([]rune(part)) > 63 || rgx.ReplaceAllString(part, "") != part {
			return nil, errors.New("invalid domain/subdomain part")
		}
	}

	obj.Domain = parts[len(parts)-1]
	parts = parts[:len(parts)-1]

	obj.Subdomains = parts

	return &obj, nil
}
